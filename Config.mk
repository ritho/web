GENPORTOFF?=0
genport = $(shell expr ${GENPORTOFF} + \( $(shell id -u) - \( $(shell id -u) / 100 \) \* 100 \) \* 200 + 20000 + 0${1})

TOPDIR?=$(realpath .)

USER=$(shell id -un)
HOST=$(shell hostname -f | head -1)

HTTP_PORT=$(call genport,1)
HTTPS_PORT=$(call genport,2)

PID_FILE=.web.pid
CONF_DIR=$(TOPDIR)/conf
STATIC_DIR=$(TOPDIR)/static

LOG_DIR=$(TOPDIR)/log
LOG_FILE=$(LOG_DIR)/web.log
LOG_PREFIX=web_

SUPPORT_EMAIL=palvarez@ritho.net

USR_BIN=/usr/bin
USR_SBIN=/usr/sbin

INSTALL=/usr/bin/install

SSL_DIR=$(CONF_DIR)/ssl
SSL_CERT=$(SSL_DIR)/server.crt
SSL_KEY=$(SSL_DIR)/priv/server.key
SSL_CONFIG=$(SSL_DIR)/openssl.cnf

export
