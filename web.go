// Package web implements the web server
package web

/* Copyright (C) 2018 Pablo Alvarez de Sotomayor Posadillo <palvarez@ritho.net>

   This file is part of web.

   web is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   web is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with web. If not, see <http://www.gnu.org/licenses/>.
*/

import (
	"fmt"
	"net"
	"time"

	"github.com/golang/glog"
	"github.com/valyala/fasthttp"

	"bitbucket.org/ritho/web/pkg/config"
	"bitbucket.org/ritho/web/pkg/controller"
)

// Web type set up the web server to response to the petitions.
type Web struct {
	cfg      *config.Config
	listener net.Listener
}

// New returns a new Web object.
func New(cfgDir string) *Web {
	glog.Infof("Reading the configurations from %s", cfgDir)
	cfg, err := config.New(cfgDir)
	if err != nil {
		glog.Warningf("Unable to read the configs from %s, trusting the default values",
			cfgDir)
	}

	return &Web{
		cfg: cfg,
	}
}

// Start the Web server.
func (w *Web) Start() error {
	err := w.cfg.ReloadConfigs()
	if err != nil {
		glog.Errorf("Error reloading the configurations: %s", err)
		return err
	}

	c := controller.New(w.cfg)
	server := fasthttp.Server{
		Handler:           fasthttp.CompressHandler(c.Router.Handler),
		ReadBufferSize:    1024 * 64,
		WriteBufferSize:   1024 * 64,
		ReduceMemoryUsage: true,
	}

	if w.listener != nil {
		return fmt.Errorf("Listener already started")
	}

	w.listener, err = net.Listen("tcp4", fmt.Sprint(":", w.cfg.Port))
	if err != nil {
		glog.Errorf("Error creating the listener: %s", err)
		return err
	}

	glog.Infof("Starting server on port %d", w.cfg.Port)
	go func() {
		err := server.Serve(w.listener)
		if err != nil {
			glog.Errorf("Error starting the server: %s", err)
		}
	}()

	return err
}

// Stop the Web server.
func (w *Web) Stop() error {
	var err error

	if w.listener != nil {
		glog.Info("Stopping the Web")
		err = w.listener.Close()
		time.Sleep(time.Second)
		w.listener = nil
	}

	return err
}

// Reload the Web server.
func (w *Web) Reload() error {
	err := w.Stop()
	if err != nil {
		glog.Errorf("Error stoping the server: %s", err)
		return err
	}

	err = w.Start()
	if err != nil {
		glog.Errorf("Error starting the server: %s", err)
	}

	return err
}
