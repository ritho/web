# Ritho web
Here you can find the source code for the ritho web located at http://ritho.net.

# How to use it

Both to build and test the project I make use of docker, using the `golang:1.10-alpine` image, that will be downloaded when you build the project or run the tests for the first time, so you need permissions to run docker containers to work with the project.

To build the **web** command for the first time you need to install *quickpages*:

``` shell
go get -u github.com/valyala/quicktemplate/qtc
```

and then, to build it just run:

```shell
make
```

and it will generate the binary in `bin/amd64/web`. To run the tests you should run:

```shell
make tests
```

and it will show the tests results on the terminal. There are also a `linter` and `coverate` targets in the `Makefile`, but to use them you need to install `gocov` and `gometalinter`:

```shell
go get -u github.com/axw/gocov/...
go get -u github.com/AlekSi/gocov-xml
go get -u github.com/alecthomas/gometalinter
gometalinter --install
```

After that, you just need to run `make coverage` and `make linter` to run the code coverage target and the linter target. You can also cross-compile **web** by changing the variable ARCH in the Makefile by one of the next:

* amd64
* arm
* arm64
* ppc64le

# License
Web is licensed under the [GNU GPLv3](https://www.gnu.org/licenses/gpl.html). You should have received a copy of the GNU General Public License along with Gotemplate. If not, see http://www.gnu.org/licenses/.

![GNU GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)
