TOPDIR?=$(realpath .)
include $(TOPDIR)/Config.mk

# The binary to build (just the basename).
BIN := web

# This repo's root import path (under GOPATH).
PKG := bitbucket.org/ritho/$(BIN)

# Where to push the docker image.
REGISTRY ?= ritho

# This version-strategy uses git tags to set the version string
VERSION := $(shell git describe --tags --always --dirty)

BUILD_IMAGE ?= golang:1.10-alpine

all: build

build: bin/$(BIN)

bin/$(BIN): build-dirs
	@echo "building: $@"
	@docker run                                                            \
	    -ti                                                                \
	    --rm                                                               \
	    -u $$(id -u):$$(id -g)                                             \
	    -v $$(pwd)/.go:/go                                                 \
	    -v $$(pwd):/go/src/$(PKG)                                          \
	    -v $$(pwd)/bin/:/go/bin                                            \
	    -v $$(pwd)/bin/:/go/bin/linux_amd64                                \
	    -v $$(pwd)/.go/std/amd64:/usr/local/go/pkg/linux_amd64_static      \
	    -w /go/src/$(PKG)                                                  \
	    $(BUILD_IMAGE)                                                     \
	    /bin/sh -c "                                                       \
	        BIN=$(BIN)                                                     \
	        PKG=$(PKG)                                                     \
	        ./scripts/build.sh                                             \
	    "

version:
	@echo $(VERSION)

tests: build-dirs
	@echo "testing: $@"
	@docker run                                                            \
	    -ti                                                                \
	    -u $$(id -u):$$(id -g)                                             \
	    -v $$(pwd)/.go:/go                                                 \
	    -v $$(pwd):/go/src/$(PKG)                                          \
	    -v $$(pwd)/bin:/go/bin                                             \
	    -v $$(pwd)/.go/std/amd64:/usr/local/go/pkg/linux_amd64_static      \
	    -w /go/src/$(PKG)                                                  \
	    $(BUILD_IMAGE)                                                     \
	    /bin/sh -c "                                                       \
	        ./scripts/test.sh                                              \
	    "

linter:
	@gometalinter --config=".gometalinter.json" --json ./...

coverage:
	@$(TOPDIR)/scripts/coverage.sh

build-dirs:
	@mkdir -p bin
	@mkdir -p .go/src/$(PKG) .go/pkg .go/bin .go/std/amd64
	@if [ ! -d $(TOPDIR)/vendors ]; then $(MAKE) update-vendors; fi;
	#@qtc -dir $(TOPDIR)/templates

clean: bin-clean files-clean

start:
	@$(MAKE) -C $(CONF_DIR) $(BIN).yaml
	@mkdir -p $(LOG_DIR)
	@if [ ! -f $(TOPDIR)/$(PID_FILE) ]; then \
		echo -n "Startng $(BIN)"; \
		$(TOPDIR)/bin/$(BIN) --logtostderr 1> $(LOG_FILE) < /dev/null 2>&1 & \
		echo $$! > $(TOPDIR)/$(PID_FILE) ; \
	fi

stop:
	@if [ -f $(TOPDIR)/$(PID_FILE) ]; then \
		echo -n "\\033[1;35m+++ Stopping web\\033[39;0m "; \
		kill -s 15 `cat $(TOPDIR)/$(PID_FILE)`; \
		rm -f $(TOPDIR)/$(PID_FILE); \
	fi

restart:
	$(MAKE) stop
	$(MAKE) start

build-local: build-dirs
	@go build -o bin/$(BIN) cmd/$(BIN)/main.go

rpm build-rpm: build-local
	@cp bin/$(BIN) rpm/
	@rpmbuild --define "_sourcedir $(TOPDIR)/rpm" -bb $(TOPDIR)/rpm/web.spec

bin-clean:
	@rm -rf .go bin

files-clean:
	@rm -fr cpu-*.log mem-*.log block-*.log web.test $(LOG_DIR)

check-cpu-tests:
	@go tool pprof -text -nodecount=10 ./web.test cpu-*.log

check-block-tests:
	@go tool pprof -text -nodecount=10 ./web.test block-*.log

check-mem-tests:
	@go tool pprof -text -nodecount=10 ./web.test mem-*.log

check-tests:
	@echo "CPU tests"
	@$(MAKE) check-cpu-tests
	@echo "Block tests"
	@$(MAKE) check-block-tests
	@echo "Mem tests"
	@$(MAKE) check-mem-tests

update-vendors:
	@dep ensure

cleanlogs:
	@for log in $(LOG_FILE); do \
		echo "" > $$log ; \
	done

help:
	@echo "\033[1;35mmake\\033[39;0m - Build the web command using docker."
	@echo "\033[1;35mmake build-local\\033[39;0m - Build the web command without using docker."
	@echo "\033[1;35mmake tests\\033[39;0m - Run the tests using docker."
	@echo "\033[1;35mmake start\\033[39;0m - starts web service."
	@echo "\033[1;35mmake stop\\033[39;0m - stops web service."
	@echo "\033[1;35mmake restart\\033[39;0m - restarts web service."

rinfo:
	@echo "Development environment: \033[1;35mhttp://$(HOST):$(HTTP_PORT)\\033[39;0m"
	@echo "Development environment SSL: \033[1;35mhttps://$(HOST):$(HTTPS_PORT)\\033[39;0m"
